# Anonymous Certification

**3-Colors Graph**

This sample project is a proof of concept of the Anonymous Certification using a 3-Colors Graph.
It allows to :

- create a 3-Colors Graph
- pawn the graph
- get a authentification certification from this graph

No plugin required, just Gradle

## Prerequisites

- Java 11 or newer


# Create the .jar archive

Execute : `gradle build` from the project root folder 

# Run it

In order to run the .jar file using default vaules, simply execute

    `java -jar build/libs/AnonymousCertification.jar`

It is possible to pass some parameters as input to the program. To know how to do so, execute 

    `java -jar build/libs/AnonymousCertification.jar - h`
