package util;


import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public abstract class Hash {

	public static String hashSha256(String pMessage) {

		StringBuffer sb = new StringBuffer();
		byte[] lMessageUTF8;
		MessageDigest lMessageDigest;

		try {
			// On convertit de le message de UNICODE vers UTF-8
			lMessageDigest = MessageDigest.getInstance("SHA-256");
			lMessageUTF8 = pMessage.getBytes("UTF-8");
		} catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
			return "Erreur encodage";
		}
		
		for (byte b : lMessageDigest.digest(lMessageUTF8)) {
			sb.append(String.format("%02x", b));
		}
		return sb.toString();
	}

	public static String hashSha1(String pMessage) {
		
		StringBuffer sb = new StringBuffer();
		byte[] lMessageUTF8;
		MessageDigest lMessageDigest;
		
		try {
			// On convertit le message de UNICODE vers UTF-8
			lMessageDigest = MessageDigest.getInstance("SHA-1");
			lMessageUTF8 = pMessage.getBytes("UTF-8");
		} catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
			return "Erreur encodage";
		}
		
		for (byte b : lMessageDigest.digest(lMessageUTF8)) {
			sb.append(String.format("%02x", b));
		}
		return sb.toString();
	}
	
	public static String byteKeyToString(byte[] iKey) {
		String stringKey = "";
		for (byte b : iKey) {
			stringKey += b;
		}
		return stringKey;
	}
}
