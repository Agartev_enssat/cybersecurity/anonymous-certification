package messages;

import prover.Colours;

public class ColoursAndHashsMessage extends Message {

	private final Colours iColour;
	private final byte[] iKey;

	private final Colours jColour;
	private final byte[] jKey;

	public ColoursAndHashsMessage(Colours iColour, Colours jColour, byte[] iKey, byte[] jKey) {
		this.iColour = iColour;
		this.jColour = jColour;
		this.iKey = iKey;
		this.jKey = jKey;
	}

	public final Colours getIColour() {
		return iColour;
	}

	public final byte[] getIKey() {
		return iKey;
	}

	public final Colours getJColour() {
		return jColour;
	}

	public final byte[] getJKey() {
		return jKey;
	}
}
