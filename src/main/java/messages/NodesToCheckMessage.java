package messages;

public class NodesToCheckMessage extends Message {

	private final int i;
	private final int j;

	public NodesToCheckMessage(int i, int j) {
		this.i = i;
		this.j = j;
	}

	public int getI() {
		return i;
	}

	public int getJ() {
		return j;
	}
}
