package messages;

public class PawnMessage extends Message{
	private final String[] pawn;

	public PawnMessage(String[] pawn) {
		this.pawn = pawn;
	}

	public String[] getPawn() {
		return pawn;
	}
}
