package messages;

public class MatrixMesage extends Message{
	private final int[][] matrix;

	public MatrixMesage(int[][] matrix) {
		this.matrix = matrix;
	}

	public int[][] getMatrix() {
		return matrix;
	}
}
