package verifier;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import messages.ColoursAndHashsMessage;
import messages.MatrixMesage;
import messages.Message;
import messages.NodesToCheckMessage;
import messages.PawnMessage;
import messages.TestAgainMessage;
import messages.TestFailedMessage;
import messages.TestPassedMessage;
import prover.Colours;
import prover.Prover;
import util.Hash;

public class Verifier implements Runnable {

	private int numberOfRepetitions;
	int[][] matrix;
	private BlockingQueue<Message> readingQueue;
	private BlockingQueue<Message> writingQueue = new LinkedBlockingQueue<>();;

	private boolean logs = false;

	public Verifier(int numberOfRepetitions) {
		this.numberOfRepetitions = numberOfRepetitions;
	}

	private Pair<Integer, Integer> chooseRandomNodes() {

		List<Pair<Integer, Integer>> nodesPairs = new ArrayList<>();
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				if (this.matrix[i][j] == 1) {
					nodesPairs.add(new Pair<Integer, Integer>(i, j));
				}
			}
		}

		int rand = (int) (Math.random() * nodesPairs.size());
		return nodesPairs.get(rand);
	}

	private boolean colouringProof(String[] pawn, int i, int j, Colours iColour, Colours jColour, byte[] iKey,
			byte[] jKey) {

		String iHash = Hash.hashSha1(Hash.byteKeyToString(iKey) + iColour);
		String jHash = Hash.hashSha1(Hash.byteKeyToString(jKey) + jColour);

		return (iHash.equals(pawn[i]) && jHash.equals(pawn[j]) && iColour != jColour);
	}



	public void setMatrix(int[][] matrix) {
		this.matrix = matrix;
	}

	public void listen(Prover prover) {
		// listen the writing queue of the prover
		this.readingQueue = prover.getWritingQueue();
	}

	public BlockingQueue<Message> getWritingQueue() {
		return this.writingQueue;
	}

	@Override
	public void run() {
		try {

			Message message = this.readingQueue.take();
			printLogs("Matrix received");
			if (message instanceof MatrixMesage) {
				this.setMatrix(((MatrixMesage) message).getMatrix());
				int i = 0;
				boolean testPassed;
				do {
					testPassed = false;
					message = this.readingQueue.take();
					if (message instanceof PawnMessage) {
						printLogs("Pawn received");
						String pawn[] = ((PawnMessage) message).getPawn();
						Pair<Integer, Integer> nodes = this.chooseRandomNodes();
						this.writingQueue.add(new NodesToCheckMessage(nodes.data1, nodes.data2));
						printLogs("Nodes to check sent");
						message = this.readingQueue.take();
						if (message instanceof ColoursAndHashsMessage) {
							printLogs("Colours and hashs received");
							int iNumber = nodes.data1;
							int jNumber = nodes.data2;
							Colours iColour = ((ColoursAndHashsMessage) message).getIColour();
							Colours jColour = ((ColoursAndHashsMessage) message).getJColour();
							byte[] iKey = ((ColoursAndHashsMessage) message).getIKey();
							byte[] jKey = ((ColoursAndHashsMessage) message).getJKey();
							testPassed = this.colouringProof(pawn, iNumber, jNumber, iColour, jColour, iKey, jKey);
							if(testPassed && i < numberOfRepetitions - 1) {
								writingQueue.add(new TestAgainMessage());
							}
							printLogs("Test " + i + " passed : " + testPassed);
							
						}
					}
					i++;
				} while (i < numberOfRepetitions && testPassed);
				
				if(testPassed) {
					writingQueue.add(new TestPassedMessage());
				} else {
					writingQueue.add(new TestFailedMessage());
				}
			}

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void setLogs(boolean logs) {
		this.logs = logs;
	}

	private void printLogs(String log) {
		if (this.logs) {
			System.err.println("Verifier : " + log);
		}
	}

	class Pair<T1, T2> {

		private final T1 data1;
		private final T2 data2;

		public Pair(T1 data1, T2 data2) {
			assert data1 != null;
			assert data2 != null;
			this.data1 = data1;
			this.data2 = data2;
		}

		public T1 getData1() {
			return data1;
		}

		public T2 getData2() {
			return data2;
		}

		@Override
		public String toString() {
			return "Pair [data1=" + data1 + ", data2=" + data2 + "]";
		}
	}
}
