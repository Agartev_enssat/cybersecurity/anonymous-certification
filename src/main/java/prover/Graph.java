package prover;

public class Graph {

	private int size;

	public int getSize() {
		return size;
	}

	public void setSize(int taille) {
		this.size = taille;
	}

	private int[][] matrix;
	private Colours[] colours;

	public Graph(int size) {
		this.size = size;
		matrix = new int[size][size];
		colours = new Colours[size];
		initColours();
		initMatrix();

	}

	private void initColours() {
		for (int i = 0; i < colours.length; i++) {
			colours[i] = getRandomColour();
		}
	}

	private void initMatrix() {
		for (int i = 0; i < colours.length; i++) {
			for (int j = i + 1; j < colours.length; j++) {
				if (colours[i] != colours[j] && Math.random() < 0.5f) {
					matrix[i][j] = 1;
					matrix[j][i] = 1;
				} else {
					matrix[i][j] = 0;
					matrix[j][i] = 0;
				}
			}
		}
	}

	private String printMatrix() {
		String matrixString = "\t";
		for (int i = 0; i < this.size; i++) {
			matrixString += i + " ";
			if (i < 10)
				matrixString += " ";
		}

		matrixString += "\n\n\n";
		for (int i = 0; i < this.size; i++) {
			matrixString += i + "\t";
			for (int j = 0; j < this.size; j++) {
				matrixString += this.matrix[i][j] + "  ";
			}
			matrixString += "\n\n";

		}
		return matrixString;
	}

	private Colours getRandomColour() {
		int num = (int) (Math.random() * 3);
		switch (num) {
		case 0:
			return Colours.RED;
		case 1:
			return Colours.BLUE;
		case 2:
			return Colours.GREEN;
		default:
			throw new Error();
		}
	}

	public Colours[] getColours() {
		return this.colours;
	}

	public int[][] getMatrix() {
		return this.matrix;
	}

	@Override
	public String toString() {
		return "Graph [taille=" + size + "\n matrice : \n" + printMatrix();
	}

}
