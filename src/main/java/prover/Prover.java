package prover;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import messages.ColoursAndHashsMessage;
import messages.MatrixMesage;
import messages.Message;
import messages.NodesToCheckMessage;
import messages.PawnMessage;
import messages.TestAgainMessage;
import messages.TestFailedMessage;
import messages.TestPassedMessage;
import util.Hash;
import verifier.Verifier;

public class Prover implements Runnable {

	private Graph graph = null;
	private BlockingQueue<Message> readingQueue;
	private BlockingQueue<Message> writingQueue = new LinkedBlockingQueue<>();

	private boolean logs = false;

	public Prover(int matrixSize) {
		this.graph = new Graph(matrixSize);
	}

	private byte[][] generate128Bitskeys(int size) {
		byte[][] keys = new byte[size][16];
		for (byte[] key : keys) {
			Random random = new Random();
			random.nextBytes(key);
		}
		return keys;
	}

	public BlockingQueue<Message> getWritingQueue() {
		return this.writingQueue;
	}

	public void listen(Verifier verifier) {
		// listen the writing queue of the verifier
		this.readingQueue = verifier.getWritingQueue();
	}

	private String[] pawnColours(byte[][] keys, Colours[] couleurs) {
		String[] hash = new String[couleurs.length];
		for (int i = 0; i < hash.length; i++) {
			hash[i] = Hash.hashSha1(Hash.byteKeyToString(keys[i]) + couleurs[i]);
		}
		return hash;
	}

	@Override
	public void run() {
		try {
			// send matrix to the verifier
			this.writingQueue.add(new MatrixMesage(this.graph.getMatrix()));
			printLogs("Matrix sent");

			Message message = null;
			do {
				printLogs("");
				printLogs("===========================================");
				printLogs("");
				byte[][] keys = generate128Bitskeys(this.graph.getSize());
				Colours[] swapedColours = swapColours(graph.getColours());
				String[] pawn = pawnColours(keys, swapedColours);
				// send the colours pawn to the verifier
				this.writingQueue.add(new PawnMessage(pawn));
				printLogs("Pawn sent");
				// read the nodes to send
				message = this.readingQueue.take();
				if (message instanceof NodesToCheckMessage) {
					printLogs("Nodes to check received");
					int i = ((NodesToCheckMessage) message).getI();
					int j = ((NodesToCheckMessage) message).getJ();
					Colours iColour = swapedColours[i];
					Colours jColour = swapedColours[j];
					// send the colours and the nodes keys
					this.writingQueue.add(new ColoursAndHashsMessage(iColour, jColour, keys[i], keys[j]));
					printLogs("Colours and keys sent");
					// receive the ack from the verifier
					message = this.readingQueue.take();
					if (message instanceof TestAgainMessage) {
						printLogs("Stage passed, run next stage");
					}
				}

			} while (message instanceof TestAgainMessage);

			if (message instanceof TestPassedMessage) {
				System.out.println("Test Passed");
			} else if (message instanceof TestFailedMessage) {
				System.out.println("Test Failed");
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private Colours[] swapColours(Colours[] couleurs) {
		List<Colours> randomisedColours = new LinkedList<>();
		Colours[] swapedColours = new Colours[couleurs.length];

		randomisedColours.add(Colours.BLUE);
		randomisedColours.add(Colours.RED);
		randomisedColours.add(Colours.GREEN);
		Collections.shuffle(randomisedColours);

		for (int i = 0; i < couleurs.length; i++) {
			switch (couleurs[i]) {
			case BLUE:
				swapedColours[i] = randomisedColours.get(0);
				break;
			case RED:
				swapedColours[i] = randomisedColours.get(1);
				break;
			case GREEN:
				swapedColours[i] = randomisedColours.get(2);
				break;

			default:
				break;
			}
		}
		return swapedColours;
	}

	public void setLogs(boolean log) {
		this.logs = log;
	}

	private void printLogs(String log) {
		if (this.logs) {
			System.err.println("Prover : " + log);
		}
	}
}