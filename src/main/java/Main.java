
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import prover.Prover;
import verifier.Verifier;

public class Main {

	private static int size;
	private static int repetitions;
	private static boolean verbose;
	private static boolean help;

	public static void main(String[] args) {

		parseArguments(args);

		if (help) {
			return;
		}

		System.out.println("Creation of a " + size + "x" + size + " matrix");
		System.out.println(repetitions + " repetitions to validate the certification");

		Prover prover = new Prover(size);
		Verifier verifier = new Verifier(repetitions);

		if (verbose) {
			prover.setLogs(true);
			verifier.setLogs(true);
		}

		verifier.listen(prover);
		prover.listen(verifier);

		Thread tProver = new Thread(prover);
		Thread tVerifier = new Thread(verifier);
		tProver.start();
		tVerifier.start();

		try {
			tProver.join();
			tVerifier.join();
		} catch (InterruptedException e2) {
			e2.printStackTrace();
		}
	}

	private static void parseArguments(String[] args) {

		try {
			Options options = new Options();
			options.addOption("h", "help", false, "Print this help message");
			options.addOption("r", "repetitions", true, "Set the number of repetitions, to check the accreditation");
			options.addOption("s", "size", true, "Set the size of the matrix");
			options.addOption("v", "verbose", false, "Use verbose logging");

			HelpFormatter helpFormatter = new HelpFormatter();
			helpFormatter.setWidth(110);

			CommandLineParser parser = new DefaultParser();
			CommandLine cmd = parser.parse(options, args);

			// help message
			if (cmd.hasOption("h")) {
				helpFormatter.printHelp("Anonymous_Certification.jar", options);
				help = true;
			} else {
				help = false;
			}

			// repetitions
			String repetitionsString = cmd.getOptionValue("r");
			if (repetitionsString == null) {
				repetitions = 400;
			} else {
				try {
					repetitions = Integer.parseInt(repetitionsString);
				} catch (NumberFormatException e) {
					System.err.println("\'" + repetitionsString + "\'" + " is not a number, default value used");
					repetitions = 400;
				}
			}

			// matrix size
			String sizeString = cmd.getOptionValue("s");
			if (sizeString == null) {
				size = 20;
			} else {
				try {
					size = Integer.parseInt(sizeString);
				} catch (NumberFormatException e) {
					System.err.println("\'" + sizeString + "\'" + " is not a number, default value used");
					size = 20;
				}
			}

			// verbose mode
			if (cmd.hasOption("v")) {
				verbose = true;
			} else {
				verbose = false;
			}
		} catch (ParseException e1) {
			System.err.println("Unrecognized option");
			System.err.println("Try option -h or --help to print the help");
		}
	}
}