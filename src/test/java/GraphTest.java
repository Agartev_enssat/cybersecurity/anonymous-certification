import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

import prover.Colours;
import prover.Graph;

public class GraphTest {
	@Test
	public void validMatrix(){
		Graph graph = new Graph(20);
		
		int[][] matrix = graph.getMatrix();
		Colours[] coulours = graph.getColours();
		for (int i = 0; i < coulours.length; i++) {
			for (int j = 0; j < coulours.length; j++) {
				if(matrix[i][j] == 1) {
					assertNotEquals(coulours[i], coulours[j]);
				}
			}
		}
	}
}
